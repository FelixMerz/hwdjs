# hwdjs

## Description

This is a software 3d renderer implemented in vanilla JavaScript (ES6). It supports wireframe and mesh view, but neither textures nor perspective projection as of now. A few primitives are provided. It performs reasonably well for smaller views, but is excruciatingly slow for full-screen views (1920*1080), sitting at seconds per frame, rather than frames per second.

If anyone is in desperate need of a software 3d renderer, this might be a last resort, for anyone whose life doesn't depend on it, you really shouldn't use it in its current form.

## Working demo

Controls:

```
click to generate a new tree
press return to switch between wireframe and mesh
Press space to stop the rotation
```

The tree generator will eventually become part of the available primitives, it's currently its own WIP project.

- [ ] [Demo on felom.de](https://felom.de/tree_0_1)
