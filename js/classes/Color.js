function rgbToCol(r, g, b){
	var col = (b << 16) | (g << 8) | (r);
	//return (255 << 24) | 0;
	return col;
}

export {rgbToCol};
