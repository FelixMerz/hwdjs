import {Vec2} from "./Vec2.js";
import {Vec3} from "./Vec3.js";
import {BoundingBox} from "./BoundingBox.js";
import {rgbToCol} from "./Color.js";

class Tri{
	constructor(p1, p2, p3, col){
		this.shade = 1;
		this.vertices = [p1, p2, p3];
		if(col == undefined){
			//col = rgbToCol(255, 0, 255);
			col = rgbToCol(Math.round(Math.random()*255), 0, 255);
		}
		this.color = col;
	}

	rotateByMatrix(mat){
		return new Tri(this.vertices[0].rotateByMatrix(mat), this.vertices[1].rotateByMatrix(mat), this.vertices[2].rotateByMatrix(mat));
	}

	getIntersectionPoint(ray){
		var E1 = this.vertices[1].sub(this.vertices[0]); // B - A
		var E2 = this.vertices[2].sub(this.vertices[0]); // C - A
		var N = E1.cross(E2); //cross(E1,E2);
		var det = -(ray.towards.dot(N)); //dot(R.Dir, N);
		if(det < 0.000001)
			return false;
		var invdet = 1.0/det;
		var AO  = ray.origin.sub(this.vertices[0]);// - A;
		var DAO = AO.cross(ray.towards);//cross(AO, R.Dir);
		var u =  E2.dot(DAO) * invdet; //dot(E2,DAO) * invdet;
		if(u < 0.0)
			return false;
		var v = -(E1.dot(DAO)) * invdet; //dot(E1,DAO) * invdet;
		if(v < 0.0)
			return false;
		var t =  AO.dot(N) * invdet; //dot(AO,N)  * invdet; 
		//if(det >= 0.000001 && t >= 0.0 && u >= 0.0 && v >= 0.0 && (u+v) <= 1.0){
		if(t >= 0.0 && (u+v) <= 1.0){
			return ray.origin.add(ray.towards.scale(t));
		}
		return false;
	}

	getVertexIndex(p){
		for(var i=0; i<3; i++){
			if(p.equals(this.vertices[i])){
				return i;
			}
		}
		return -1;
	}

	snap(){
		var points = [];
		for(var i=0; i<3; i++){
			var x = Math.round(this.vertices[i].x);
			var y = Math.round(this.vertices[i].y);
			var z = Math.round(this.vertices[i].z);
			points.push(new Vec3(x, y, z));
		}

		return new Tri(points[0], points[1], points[2]);
	}

	move(v){
		return new Tri(this.vertices[0].add(v), this.vertices[1].add(v), this.vertices[2].add(v));
	}

	orthogonal(){
		var Q = this.vertices[1].sub(this.vertices[0]);
		var R = this.vertices[2].sub(this.vertices[1]);

		//var Q = this.vertices[0].sub(this.vertices[1]);
		//var R = this.vertices[1].sub(this.vertices[2]);
		return Q.cross(R);
	}


	//returns MAX_DISTANCE when z-component is 0 (plane spans on either xz or yz). It's geometrically invisible and will be treated as such.
	getZ(vx, vy){
		if(this.orthogonal().z == 0)
			return MAX_DISTANCE;
		var perp = this.vertices[0].sub(this.vertices[1]).cross(this.vertices[2].sub(this.vertices[1]));

		var P = this.vertices[0];

		var a = perp.x;
		var b = perp.y;
		var c = perp.z;

		var x = P.x;
		var y = P.y;
		var z = P.z;

		//returns r

		return -((((-a*(x-vx)) - (b*(y - vy))) / c) - z);
	}

	closestZ(){
		return Math.min(this.vertices[0].z, this.vertices[1].z, this.vertices[2].z, )
	}

	//optimized, z always 0 -> so cross product opzimized away, third dimension completely optimized away, vector math optimized away
	hasPoint(x, y){
		var A = this.vertices[0];
		var B = this.vertices[1];
		var C = this.vertices[2];

		var c1 = (B.x - A.x) * (y - A.y) - (B.y - A.y) * (x - A.x);
		var s = Math.sign(c1);
		var c2 = (C.x - B.x) * (y - B.y) - (C.y - B.y) * (x - B.x);
		if(s != Math.sign(c2)){
			return false;
		}
		var c3 = (A.x - C.x) * (y - C.y) - (A.y - C.y) * (x - C.x);
		if(s != Math.sign(c3))
			return false
		return true;

		/*
		var P = new Vec2(v.x, v.y);
		var A = new Vec2(this.vertices[0].x, this.vertices[0].y);
		var B = new Vec2(this.vertices[1].x, this.vertices[1].y);
		var C = new Vec2(this.vertices[2].x, this.vertices[2].y);

		var AB = new Vec2(B.x - A.x, B.y - A.y);
		var BC = new Vec2(C.x - B.x, C.y - B.y);
		var CA = new Vec2(A.x - C.x, A.y - C.y);

		var AP = new Vec3(P.x - A.x, P.y - A.y);
		var BP = new Vec3(P.x - B.x, P.y - B.y);
		var CP = new Vec3(P.x - C.x, P.y - C.y);
		*/

		// var c1 = Math.sign(AB.x * AP.y - AB.y * AP.x) | 1;
		// var c2 = Math.sign(BC.x * BP.y - BC.y * BP.x) | 1;
		// var c3 = Math.sign(CA.x * CP.y - CA.y * CP.x) | 1;

		// if(c1 == c2 && c2 == c3)
			// return true;

		//if(s == Math.sign(c2) && s == Math.sign(c3))
		//	return true;
		/*if(c1 <= 0 && c2 <= 0 && c3 <= 0)
			return true;
		if(c1 >= 0 && c2 >= 0 && c3 >= 0)
			return true;
		return false;*/
	}

	boundingBox(){
		/*
		return new BoundingBox(Math.min(this.vertices[0].x, this.vertices[1].x, this.vertices[2].x)
							  ,Math.min(this.vertices[0].y, this.vertices[1].y, this.vertices[2].y)
							  ,0
							  ,Math.max(this.vertices[0].x, this.vertices[1].x, this.vertices[2].x)
					  		  ,Math.max(this.vertices[0].y, this.vertices[1].y, this.vertices[2].y)
							  ,0
						  );
		*/
		var xmin = this.vertices[0].x;
		var ymin = this.vertices[0].y;
		var xmax = xmin;
		var ymax = ymin;

		for(var i=0; i<this.vertices.length; i++){
			var x = this.vertices[i].x;
			var y = this.vertices[i].y;
			if(x < xmin){
				xmin = x;
			}else if(x > xmax){
				xmax = x;
			}
			if(y < ymin){
				ymin = y;
			}else if(y > ymax){
				ymax = y;
			}
		}
		//this.bb = new BoundingBox(xmin, ymin, 0, xmax, ymax, 0);
		return new BoundingBox(xmin, ymin, 0, xmax, ymax, 0);
	}

	normal(){
		return this.vertices[1].sub(this.vertices[0]).cross(this.vertices[2].sub(this.vertices[0]));
	}

	extrude(direction){
		if(!direction){
			direction = this.normal();
		}
		var newTris = [];
		var newVertices = new Array(3);
		for(var i=0; i<3; i++){
			newVertices[i] = this.vertices[i].add(direction);
		}
		//"ring"
		for(var i=0; i<3; i++){
			newTris.push(new Tri(this.vertices[i], newVertices[i], newVertices[(i+1)%3]));
			newTris.push(new Tri(this.vertices[i], this.vertices[(i+1)%3], newVertices[(i+1)%3]));
		}
		//"top"
		newTris.push(new Tri(newVertices[0], newVertices[1], newVertices[2]));
		console.log("newTris", newTris);
		return newTris;
	}
}

export {Tri};
