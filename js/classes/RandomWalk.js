import {Mesh} from "./Mesh.js";
import {UnitCube} from "./UnitCube.js";
import {Vec3} from "./Vec3.js";

class RandomWalk extends Mesh{
	constructor(steps, scale, loops=true, fromString=""){
		super();
        var start = new UnitCube().scale(0.6);
        /*var marker = new UnitCube().scale(1.1);
        this.triangles = marker.getTranslatedTriangles().concat(start.getTranslatedTriangles());*/
        this.triangles = start.getTranslatedTriangles();
		if(fromString != ""){
			this.rw = fromString;
		}else if(loops){
    		this.rw = this.generateRandomLoop(steps);
    	}else{
    		this.rw = this.generateRandomWalk(steps);
    	}
        //console.log(this.rw);
        var pos = new Vec3(0, 0, 0);
		var shrinker = 0.01;
        for(var i=0; i<this.rw.length; i++){
            for(var j=0; j<scale*2; j++){
                var cube = new UnitCube();
				var offset = new Vec3(0, 0, 0);
                switch(this.rw[i]){
                    case "l":
                        offset.x -= 0.5;
                        break;
                    case "r":
                        offset.x += 0.5;
                        break;
                    case "u":
                        offset.y -= 0.5;
                        break;
                    case "d":
                        offset.y += 0.5;
                        break;
                    case "f":
                        offset.z -= 0.5;
                        break;
                    case "b":
                        offset.z += 0.5;
                        break;
                }
				for(var k=0; k<6; k++){
					if(cube.triangles[k].normal().normalize == offset.normalize()){
						cube.triangles.splice(k*2, 2);
					}
				}
                cube.scale(0.49 + (shrinker));
				shrinker *= 0.99999;
				pos = pos.add(offset);
                cube.shift(pos);
                this.triangles = this.triangles.concat(cube.triangles);
            }
        }
    }

	generateRandomWalk(steps){
	    var walk = "";
		var directions = "udlrfb";
	    for(var i=0; i<steps; i++){
			walk += directions[Math.floor(Math.random()*6)];
	    }
	    return walk;
	}

	generateRandomLoop(steps){
	    var walk = "";
	    for(var i=0; i<steps/2; i++){
	        var dir = Math.floor(Math.random()*3);
	        switch(dir){
	            case 0:
	                walk+="lr";
	                break;
	            case 1:
	                walk+="ud";
	                break;
	            case 2:
	                walk+="fb";
	                break;
	        }
	    }
	    return this.shuffleArray(Array.from(walk)).toString().replaceAll("," ,"");
	}

	generateAllWalks(n, nx, ny, resx, resy){
		this.triangles = [];
		var loopers = [];
		var nWalks = Math.pow(6, n);
		var walks = new Array(nWalks);
		for(var i=0; i<nWalks; i++){
			var walk = i.toString(6);
			while(walk.length < n){
				walk = "0" + walk;
			}
			walk = walk.replaceAll("0", "u");
			walk = walk.replaceAll("1", "d");
			walk = walk.replaceAll("2", "l");
			walk = walk.replaceAll("3", "r");
			walk = walk.replaceAll("4", "b");
			walk = walk.replaceAll("5", "f");
			walks[i] = walk;
		}
		console.log(walks.toString());
		for(var i=0; i<nWalks; i++){
			var w = new RandomWalk(walks[i].length, 1, false, walks[i]);
			if(w.loops()){
				loopers.push(i);
				console.log(w.triangles.length);
			}

			//w.scale(2);
			w.scale(5);
			//w.scale(20);

			w.rotate(30, 1, 0, 0);
			w.rotate(46, 0, 1, 0);

			var marginx = 30;
			var marginy = 30;

			var yoff = marginy + (i%ny) * ((resy-marginy*2)/(ny-1));
			var xoff = marginx + Math.floor(i/(ny)) * ((resx-marginx*2)/(nx-1));

			w.move(new Vec3(xoff, yoff, 0));

			this.triangles = this.triangles.concat(w.getTranslatedTriangles());
		}
		console.log(loopers.toString());
		return walks;
	}

	shuffleArray(arr){
	    var narr = new Array(arr.length);
	    var maxit = arr.length;
	    for(var i=0; i<maxit; i++){
	        var pick = Math.floor(Math.random()*arr.length);
	        narr[i] = arr[pick];
	        arr.splice(pick, 1);
	    }
	    return narr;
	}

	loops(){
		var fixrw = this.rw + "udlrbf";
		if(fixrw.match(/u/g).length == fixrw.match(/d/g).length
		&& fixrw.match(/l/g).length == fixrw.match(/r/g).length
		&& fixrw.match(/f/g).length == fixrw.match(/b/g).length){
			return true;
		}
		return false;
	}
}

export {RandomWalk};
