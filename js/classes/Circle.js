import {Mesh} from "./Mesh.js";
import {Vec3} from "./Vec3.js";
import {Quaternion} from "./Quaternion.js";
import {Tri} from "./Tri.js";

class Circle extends Mesh{
    constructor(radius, segments){
        super();
        var base = new Vec3(0, 0, 0);
        var hand = new Vec3(radius, 0, 0);
        var rotation = new Quaternion(1, 0, 0, 0);
        var vertices = [hand];
        for(var i=1; i<segments; i++){
            rotation = rotation.rotate(360/segments, 0, 1, 0);
            vertices.push(hand.rotateByMatrix(rotation.toMatrix()));
        }
        console.log(vertices);
        for(var i=0; i<segments; i++){
            console.log(i);
            this.triangles.push(new Tri(base, vertices[i], vertices[(i+1)%segments]));
        }
    }
}

export {Circle};
