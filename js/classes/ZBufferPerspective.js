import {Vec2} from "./Vec2.js";

class ZBuffer{
	constructor(w, h){
		this.w = w;
		this.h = h;
		this.z = new Array(this.w*this.h);
		this.colors = new Uint32Array(this.w*this.h).fill(0x0a141eff);
		this.dim = this.w*this.h*4;
	}

	populateFromGeometry(tris, rays){
		var buf = new ArrayBuffer(this.dim);
		var triIndices = new Array(this.w*this.h);
		this.colors = new Uint32Array(buf).fill(BACKGROUND_COLOR);
		this.z.fill(MAX_DISTANCE);
		var n = performance.now();
		for(var i=0; i<rays.length; i++){
			for(var j=0; j<tris.length; j++){
				//console.log("iterating j");
				var p = tris[j].getIntersectionPoint(rays[i]);
				if(p){
				//	console.log("p true");
					var d = p.distance(rays[i].origin);
					if(d < this.z[i]){
						this.z[i] = d;
						triIndices[i] = j;
						//var r = ((tris[j].color) & 0x000000ff) * Math.max(0.5, tris[j].shade);
						//var g = ((tris[j].color >> 8) & 0x000000ff) * Math.max(0.5, tris[j].shade);
						//var b = ((tris[j].color >> 16) & 0x000000ff) * Math.max(0.5, tris[j].shade);
						//this.colors[i] = 0xff000000 | (b << 16) | (g << 8) | r;
					}
				}
			}
		}
		console.log("populateFromGeometry nested loop:", performance.now()-n);
		//pull colors from triangles all at once instead of in forforfor
		for(var i=0; i<triIndices.length; i++){
			if(triIndices[i] != null){		
				var ind = triIndices[i];	
				var r = ((tris[ind].color) & 0x000000ff) * Math.max(0.5, tris[ind].shade);
				var g = ((tris[ind].color >> 8) & 0x000000ff) * Math.max(0.5, tris[ind].shade);
				var b = ((tris[ind].color >> 16) & 0x000000ff) * Math.max(0.5, tris[ind].shade);
				this.colors[i] = 0xff000000 | (b << 16) | (g << 8) | r;
			}
		}
		return [new Uint8ClampedArray(buf), 0];
	}
}

export {ZBuffer};
