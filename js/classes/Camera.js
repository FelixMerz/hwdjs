import {Quaternion} from "./Quaternion.js";
import {Ray} from "./Ray.js";
import {Vec3} from "./Vec3.js";
import {Mesh} from "./Mesh.js";
import {Tri} from "./Tri.js";

class Camera{
	constructor(origin, towards, resx, resy, fov, cutoff){
		this.origin = origin;
		this.towards = towards;
		this.fov = fov;
		this.resx = resx;
		this.resy = resy;
		this.cutoff = cutoff;
		this.rays = this.generateRays();
		this.plane = this.generatePlane();
		this.c = document.createElement("canvas");
		this.c.width = resx;
		this.c.height = resy;
		this.c.style.width = Math.max(500, resx) + "px";
		this.c.style.height = Math.max(250, resy) + "px";
		this.c.style.imageRendering = "pixelated";
		this.ctx = this.c.getContext("2d");
		document.body.appendChild(this.c);
	}
	
	generatePlane(){
		var p = new Mesh(0);
		var e = new Vec3(0, 0, 0);
		var p1 = this.rays[0].towards.add(e);
		var p2 = this.rays[this.resx-1].towards.add(e);
		var p3 = this.rays[this.resy*this.resx-1].towards.add(e);
		var p4 = this.rays[this.resx*this.resy-this.resx].towards.add(e);
		var t1 = new Tri(p1, p2, p3);
		var t2 = new Tri(p1, p2, p4);
		p.triangles = [t1, t2];
		return p.scale(this.cutoff);
	}

	generateRays(){
		var hdeg = this.fov / this.resx;
		var vdeg = (this.fov * (this.resy / this.resx)) / this.resy;
		var r = new Array(this.resx * this.resy);
		var offsetHor = -hdeg*this.resx/2;
		var offsetVer = -vdeg*this.resy/2;
		var q = new Quaternion(0, 0, 1, 0);
		for(var j=0; j<this.resy; j++){
			for(var i=0; i<this.resx; i++){
				var horQ = q.rotate(offsetHor + hdeg*i, 0, 0, 1);
				var verQ = q.rotate(offsetVer + vdeg*j, 1, 0, 0);
				var rot = horQ.multiply(verQ);
				var d = this.towards.rotateByMatrix(rot.toMatrix());
				r[j*this.resx+i] = new Ray(this.origin, d);
			}
		}
		return r;
	}
}

export {Camera};
