import {Vec3} from "./Vec3.js";
import {Tri} from "./Tri.js";
import {Mesh} from "./Mesh.js";

class Terrain extends Mesh{
	constructor(l, xel, yel, noise){
		super();

		var vertices = []
		for(var i=0; i<xel; i++){
			for(var j=0; j<yel; j++){
				vertices.push(new Vec3(i*l, noise.poll(i*l, j*l), j*l));
				if(i!=0 && j!=0){
					this.triangles.push(new Tri(vertices[i*xel+j], vertices[(i-1)*xel+j], vertices[i*xel+(j-1)]));
					this.triangles.push(new Tri(vertices[(i-1)*xel+j], vertices[(i-1)*xel+(j-1)], vertices[(i)*xel+(j-1)]));
				}
			}
		}
	}
}

export {Terrain};
