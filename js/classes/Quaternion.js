import {Matrix} from "./Matrix.js";

class Quaternion{
	constructor(w, x, y, z){
		this.w = w;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	magnitude(){
		return Math.sqrt(this.w*this.w, this.x*this.x, this.y*this.y, this.z*this.z);
	}

	normalize(){
		var mag = this.magnitude();
		return new Quaternion(this.w / mag, this.x / mag, this.y / mag, this.z / mag);
	}

	//Q1 * Q2 != Q2 * Q1
	multiply(q){
		return new Quaternion(
			this.w*q.w - this.x*q.x - this.y*q.y - this.z*q.z,
			this.w*q.x + this.x*q.w + this.y*q.z - this.z*q.y,
			this.w*q.y - this.x*q.z + this.y*q.w + this.z*q.x,
			this.w*q.z + this.x*q.y - this.y*q.x + this.z*q.w
		);
	}

	rotate(angle, x, y, z){
		var deg = angle/180*Math.PI;
		var rotationQuaternion = new Quaternion(Math.cos(deg/2), x * Math.sin(deg/2), y * Math.sin(deg/2), z * Math.sin(deg/2));
		return this.multiply(rotationQuaternion);//.multiply(this);
	}

	toMatrix(){
		var w = this.w;
		var x = this.x;
		var y = this.y;
		var z = this.z;
		return new Matrix(
		[
		[1-2*y*y - 2*z*z, 2*x*y + 2*z*w, 2*x*z - 2*y*w, 0],
		[2*x*y - 2*z*w, 1 - 2*x*x - 2*z*z, 2*y*z + 2*x*w, 0],
		[2*x*z + 2*y*w, 2*y*z - 2*x*w, 1 - 2*x*x - 2*y*y, 0],
		[0, 0, 0, 1]
		]
		);
	}
}

export {Quaternion};
