import {Mesh} from "./Mesh.js";
import {UnitCube} from "./UnitCube.js";
import {Vec3} from "./Vec3.js";
import {Quaternion} from "./Quaternion.js";
import {Tri} from "./Tri.js";

class PineTree extends Mesh{
	constructor(height, segments, edges){
		super();

		var gr = 1.61803398875;
		var width = height/3;
		var seq = fib(segments+1);
		var total = sum(fib(segments))-1;
		for(var i=0; i<segments; i++){
			if(i != segments-1){
				var segheight = (height/total)*seq[segments-i];
				//console.log("segheight", segheight);
				var test = this.makeSegment(segheight, edges, segheight, false, i*60);
				this.triangles = this.triangles.concat(test);
			}
		}
		console.log(this.triangles[0].vertices[0], this.triangles[0].vertices[1], this.triangles[0].vertices[2]);
	}

	makeSegment(height, edges, yoff, top, col){
		var topvertices = [];
		var botvertices = [];
		var tris = [];
		var rot = new Quaternion(1, 0, 0, 0);
		if(!top)
			yoff-=height*0.1;
		console.log("yoff", yoff);
		console.log("height", height);
		for(var i=0; i<edges; i++){
			var botbasevert = new Vec3(0, yoff, height);
			var topbasevert = new Vec3(0, yoff-50, height/5);
			var rotmat = rot.rotate((360/edges)*i, 0, 1, 0).toMatrix();
			botvertices.push(botbasevert.rotateByMatrix(rotmat));
			topvertices.push(topbasevert.rotateByMatrix(rotmat));
			if(i!=0){
				var t1 = new Tri(topvertices[i-1], topvertices[i], botvertices[i-1], 0xff000000 | (col << 16) | (col << 8) | col);
				var t2 = new Tri(botvertices[i-1], topvertices[i], botvertices[i], 0xff000000 | (col << 16) | (col << 8) | col);
				tris.push(t1);
				tris.push(t2);
			}
		}
		// for(var i=0; i<edges; i++){
			// var basevert = new Vec3(0, -yoff, height/3);
			// topvertices.push(basevert.add(new Vec3(0, height, 0)).rotateByMatrix(rot.rotate(360/edges*i, 0, 1, 0).toMatrix()));
			// botvertices.push(basevert.scale(0.01).rotateByMatrix(rot.rotate(360/edges*i, 0, 1, 0).toMatrix()));
			// botvertices[botvertices.length-1].y = height;
			// if(i!=0){
				// tris.push(new Tri(topvertices[i-1], topvertices[i], botvertices[i-1]));
				// tris.push(new Tri(botvertices[i-1], topvertices[i], botvertices[i]));
			// }
		// }
		tris.push(new Tri(topvertices[edges-1], topvertices[0], botvertices[edges-1]));
		tris.push(new Tri(botvertices[edges-1], topvertices[0], botvertices[0]));
		var c = new UnitCube();
		c.origin.y = yoff;
		c.scale(25);
		tris = tris.concat(c.triangles);
		return tris;
	}
}

export {PineTree};
