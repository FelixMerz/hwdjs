import {Vec2} from "./Vec2.js";
import {Vec3} from "./Vec3.js";
import {Tri} from "./Tri.js";
import {Mesh} from "./Mesh.js";

class UnitSquare extends Mesh{
	constructor(){
		super();
		var points = [];
		var initialPoint = new Vec2(1, 0).rotate(45);
		for(var i=0; i<4; i++){
			var r = initialPoint.rotate(90*i)
			points.push(new Vec3(Math.round(r.x), Math.round(r.y), 0));
		}

		this.triangles.push(new Tri(points[0], points[1], points[2]));
		this.triangles.push(new Tri(points[2], points[3], points[0]));
		this.mergeVertices();
	}
}

export {UnitSquare};
