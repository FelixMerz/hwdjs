class Comp{
	constructor(a, b){
		this.a = a;
		this.b = b;
	}

	equals(comp){
		if(this.a == comp.a && this.b == comp.b)
			return true;
		return false;
	}

	isConjugate(comp){
		if(this.a == comp.a && this.b == -comp.b)
			return true;
		return false;
	}

	add(comp){
		return new Comp(this.a+comp.a, this.b+comp.b);
	}

	scale(n){
		return new Comp(this.a*n, this.b*n);
	}

	multiply(comp){
		if(this.isConjugate(comp))
			return this.a*this.a+this.b*this.b;
		return new Comp(this.a*comp.a - this.b*comp.b, this.a*comp.b+this.b*comp.a);
	}

	divide(comp){
		return new Comp((this.a*comp.a+this.b*comp.b)/(comp.a*comp.a+comp.b*comp.b), (this.b*comp.a-this.a*comp.b)/(comp.a*comp.a+comp.b*comp.b));
	}

	conjugate(){
		return new Comp(this.a, -this.b);
	}

	magnitude(){
		return Math.sqrt(this.multiply(this.conjugate()));
	}

	toString(){
		return this.a + "+" + this.b + "i";
	}
}

export {Comp};
