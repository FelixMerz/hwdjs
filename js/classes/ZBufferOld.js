import {Vec2} from "./Vec2.js";

class ZBuffer{
	constructor(w, h, aaLevel, yoff = 0){
		this.aa = false;//false;//TODO THIS SHOULD BE A PARAMETER
		this.w = w;
		this.h = h;
		if(aaLevel){
			this.aa = true;
		}
		this.z = [new Array(this.w*this.h)];
		this.colors = [];
		this.colors.push(new Uint32Array(this.w*this.h).fill(0x0a141eff));
		//this.colors = [];
		this.dim = this.w*this.h*4;
		this.yoff = yoff;
		this.aaOff = 0.4;
		this.aaLevel = aaLevel;
		if(this.aa){
			for(var i=0; i<this.aaLevel-1; i++){
				this.z.push(new Array(this.w*this.h));
				this.colors.push(new Uint32Array(this.w*this.h).fill(0x0a141eff));
			}
		}
	}

	populateFromGeometryThreaded(tris, aa){
		var nThreads = navigator.hardwareConcurrency;
		var defaultHeight = Math.floor(this.h / nThreads);
		var overScanHeight = this.h - (nThreads-1) * defaultHeight;

	}

	populateFromGeometryThreadedWorker(tris, aa, w){

	}

	populateFromGeometry(tris, aa){
		var actualIterations = 0;
		var buf = [new ArrayBuffer(this.dim)];
		this.colors = [new Uint32Array(buf[0]).fill(BACKGROUND_COLOR)];//0a141eff)
		this.z[0].fill(MAX_DISTANCE);
		if(this.aa && aa){
			for(var i=0; i<this.aaLevel-1; i++){
				buf.push(new ArrayBuffer(this.dim));
				this.colors.push(new Uint32Array(buf[i+1]).fill(BACKGROUND_COLOR));
				this.z[i+1].fill(MAX_DISTANCE);
			}
		}
		var aait = 1;
		if(this.aa && aa){
			aait = this.aaLevel;
		}
		var aaOffVec = new Vec2(0, 0);
		if(this.aa && aa){
			aaOffVec = new Vec2(-0.707 * this.aaOff, -0.707 * this.aaOff);
		}
		for(var aac=0; aac < aait; aac++){
			aaOffVec = aaOffVec.rotate(360/this.aaLevel);
			for(var t=0; t<tris.length; t++){
					var bb = tris[t].boundingBox();
					var xmin = bb.xmin-1;
					var xmax = bb.xmax+1;
					var ymin = bb.ymin-1;
					var ymax = bb.ymax+1;


					if(bb.xmax > 0 && bb.ymax > this.yoff && bb.xmin < this.w && bb.ymin < this.h+this.yoff){
						var closestZ = tris[t].closestZ();
						if(bb.xmin < 0)
							xmin = 0;
						if(bb.xmax > this.w)
							xmax = this.w;
						if(bb.ymin < this.yoff)
							ymin = this.yoff;
						if(bb.ymax > this.h+this.yoff)
							ymax = this.h+this.yoff;



						for(var j=ymin; j<ymax; j++){
						var partialZind = (j+this.yoff)*this.w;
							for(var i=xmin; i<xmax; i++){
								actualIterations++;
								//here
								var zind = partialZind + i;
								if(closestZ < this.z[aac][zind]){
									if(tris[t].hasPoint(i+aaOffVec.x, j+aaOffVec.y)){
										var z = tris[t].getZ(i+aaOffVec.x, j+aaOffVec.y);
										if(z < this.z[aac][zind]){
											this.z[aac][zind] = z;
											var r = ((tris[t].color) & 0x000000ff) * tris[t].shade;
											var g = ((tris[t].color >> 8) & 0x000000ff) * tris[t].shade;
											var b = ((tris[t].color >> 16) & 0x000000ff) * tris[t].shade;
											this.colors[aac][zind] = 0xff000000 | (b << 16) | (g << 8) | r;
										}
									}
								}
							}
						}
					}
			}
		}
		//combine colors
		if(this.aa && aa){
			for(var ind=0; ind < this.w*this.h; ind++){
				var r = this.colors[0][ind] & 0x000000ff;
				var g = (this.colors[0][ind] >> 8) & 0x000000ff;
				var b = (this.colors[0][ind] >> 16) & 0x000000ff;
				for(var i=1; i<this.aaLevel; i++){
					r += this.colors[i][ind] & 0x000000ff;
					g += (this.colors[i][ind] >> 8) & 0x000000ff;
					b += (this.colors[i][ind] >> 16) & 0x000000ff;
				}
				this.colors[0][ind] = 0xff000000 | (Math.floor(b/this.aaLevel) << 16) | (Math.floor(g/this.aaLevel) << 8) | Math.floor(r/this.aaLevel);
			}
		}
		console.log("Actual Iterations:", actualIterations, "(" + actualIterations/1000000 + "M)");
		return [new Uint8ClampedArray(buf[0]), this.yoff];
	}
	//why is this slower ffs
	populateFromGeometry2(rawtris, aa){
		//drop off-screen tris
		var tris = [];
		for(var i=0; i<rawtris.length; i++){
			var bb = rawtris[i].boundingBox();
			var xmin = bb.xmin-1;
			var xmax = bb.xmax+1;
			var ymin = bb.ymin-1;
			var ymax = bb.ymax+1;


			if(bb.xmax > 0 && bb.ymax > this.yoff && bb.xmin < this.w && bb.ymin < this.h+this.yoff){
				tris.push(rawtris[i]);
			}
		}
		var actualIterations = 0;
		var buf = [new ArrayBuffer(this.dim)];
		this.colors = [new Uint32Array(buf[0]).fill(BACKGROUND_COLOR)];//0a141eff)
		this.z[0].fill(MAX_DISTANCE);
		if(this.aa && aa){
			for(var i=0; i<this.aaLevel-1; i++){
				buf.push(new ArrayBuffer(this.dim));
				this.colors.push(new Uint32Array(buf[i+1]).fill(BACKGROUND_COLOR));
				this.z[i+1].fill(MAX_DISTANCE);
			}
		}
		var aait = 1;
		if(this.aa && aa){
			aait = this.aaLevel;
		}
		var aaOffVec = new Vec2(0, 0);
		if(this.aa && aa){
			aaOffVec = new Vec2(-0.707 * this.aaOff, -0.707 * this.aaOff);
		}
		for(var aac=0; aac < aait; aac++){
			aaOffVec = aaOffVec.rotate(360/this.aaLevel);
			for(var j=0; j<this.h; j++){
				var partialZind = (j+this.yoff)*this.w;
				for(var i=0; i<this.w; i++){
					var zind = partialZind + i;
					for(var t=0; t<tris.length; t++){
						actualIterations++;
						var closestZ = tris[t].closestZ();
						if(closestZ < this.z[aac][zind]){
							if(tris[t].hasPoint(i+aaOffVec.x, j+aaOffVec.y)){
								var z = tris[t].getZ(i+aaOffVec.x, j+aaOffVec.y);
								if(z < this.z[aac][zind]){
									this.z[aac][zind] = z;
									var r = ((tris[t].color) & 0x000000ff) * tris[t].shade;
									var g = ((tris[t].color >> 8) & 0x000000ff) * tris[t].shade;
									var b = ((tris[t].color >> 16) & 0x000000ff) * tris[t].shade;
									this.colors[aac][zind] = 0xff000000 | (b << 16) | (g << 8) | r;
								}
							}
						}
					}
				}
			}
		}
		//combine colors
		if(this.aa && aa){
			for(var ind=0; ind < this.w*this.h; ind++){
				var r = this.colors[0][ind] & 0x000000ff;
				var g = (this.colors[0][ind] >> 8) & 0x000000ff;
				var b = (this.colors[0][ind] >> 16) & 0x000000ff;
				for(var i=1; i<this.aaLevel; i++){
					r += this.colors[i][ind] & 0x000000ff;
					g += (this.colors[i][ind] >> 8) & 0x000000ff;
					b += (this.colors[i][ind] >> 16) & 0x000000ff;
				}
				this.colors[0][ind] = 0xff000000 | (Math.floor(b/this.aaLevel) << 16) | (Math.floor(g/this.aaLevel) << 8) | Math.floor(r/this.aaLevel);
			}
		}
		console.log("Actual Iterations:", actualIterations, "(" + actualIterations/1000000 + "M)");
		return [new Uint8ClampedArray(buf[0]), this.yoff];
	}
}

export {ZBuffer};
