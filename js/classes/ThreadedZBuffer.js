import {Vec2} from "./Vec2.js";

class ZBuffer{
	constructor(w, h, aaLevel, maxWorkers=12){
		this.aa = false;
		this.aaLevel = aaLevel;
		this.workers = Math.min(navigator.hardwareConcurrency, maxWorkers);
		if(aaLevel){
			this.aa = true;
		}
		this.w = w;
		this.h = h;
		this.aaOff = 0.4;
		this.offsets = new Array(Math.floor(this.h/this.workers));
		this.canvases = new Array(this.offsets.length);
		var remaining = w;
		for(var i=0; i<this.offsets.length; i++){
			if(i!=this.offsets.length-1){
				this.offsets[i] = Math.floor(w/this.workers);
				remaining -= this.offsets[i];
			}else{
				this.offsets[i] = remaining;
			}
			this.canvases[i] = document.createElement("canvas");
			this.canvases[i].width = this.w;
			this.canvases[i].height = this.offsets[i];
		}
		this.waitingForDraw = false;
	}

	populateFromGeometry(tris, yoffset, height){
		this.waitingForDraw = true;
		var message = {tris: tris
					  ,aa: this.aa
					  ,aaLevel: this.aaLevel
					  ,yoffset: yoffset
					  ,w: this.w
					  ,h: height
		};
		console.log(JSON.stringify(message));
	}
}

export {ZBuffer};
