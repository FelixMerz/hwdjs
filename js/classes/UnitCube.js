import {Quaternion} from "./Quaternion.js";
import {Vec3} from "./Vec3.js";
import {Mesh} from "./Mesh.js";
import {UnitSquare} from "./UnitSquare.js";
import {rgbToCol} from "./Color.js";

class UnitCube extends Mesh{
	constructor(){
		super();

		var rotation = new Quaternion(1, 0, 0, 0);
		var face = new UnitSquare();
		face.shift(new Vec3(0, 0, 1));

		//4 faces around x axis
		for(var i=0; i<4; i++){
			this.triangles.push(face.triangles[0].rotateByMatrix(rotation.toMatrix()).snap());
			this.triangles.push(face.triangles[1].rotateByMatrix(rotation.toMatrix()).snap());
			rotation = rotation.rotate(90, 1, 0, 0);
		}

		//reset rotation, rotate once around y axis, right side
		rotation = new Quaternion(1, 0, 0, 0).rotate(90, 0, 1, 0);
		this.triangles.push(face.triangles[0].rotateByMatrix(rotation.toMatrix()).snap());
		this.triangles.push(face.triangles[1].rotateByMatrix(rotation.toMatrix()).snap());

		//further rotate, left side
		rotation = rotation.rotate(180, 0, 1, 0);
		this.triangles.push(face.triangles[0].rotateByMatrix(rotation.toMatrix()).snap());
		this.triangles.push(face.triangles[1].rotateByMatrix(rotation.toMatrix()).snap());

		//clean up vertex cloud
		this.mergeVertices();
	}
}

export {UnitCube};
