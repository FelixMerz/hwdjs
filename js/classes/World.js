//import {ZBuffer} from "./ZBuffer.js";
import {ZBuffer} from "./ZBufferPerspective.js";
import {Camera} from "./Camera.js";
import {Vec3} from "./Vec3.js";

class World{
	constructor(renderw, renderh){
		var nThreads = 1;//navigator.hardwareConcurrency;
		this.c = new Camera(new Vec3(0, 0, 0), new Vec3(0, 1, 0), renderw, renderh, 110, 10);
		this.zBuffers = [];
		this.zBuffers.push(new ZBuffer(renderw, renderh));
		var lg = 0;
		for(var i=0; i<nThreads; i++){
			lg+=this.zBuffers[i].h;
		}
		this.meshes = [];
		this.ctx = this.c.ctx;
		//buffer canvas (not part of document)
		var c = document.createElement("canvas");
		c.width = this.ctx.canvas.width;
		c.height = this.ctx.canvas.height;

		this.bufferctx = c.getContext("2d");
		this.drawdata = new Uint8ClampedArray(this.ctx.canvas.width * this.ctx.canvas.height * 4);
	}

	getTranslatedTriangles(){
		var triangles = [];
		for(var i=0; i<this.meshes.length; i++){
			triangles = triangles.concat(this.meshes[i].getTranslatedTriangles());
		}
		//CAMERA HARD CODE DISABLED!!!!! TODO
		if(false && this.camera != undefined){
			for(var i=0; i<triangles.length; i++){
				for(var j=0; j<3; j++){
					this.camera.project(triangles[i].vertices[j]);
				}
			}
		}
		return triangles;
	}

	getUnculledTranslatedTriangles(){
		var triangles = [];
		for(var i=0; i<this.meshes.length; i++){
			triangles = triangles.concat(this.meshes[i].getUnculledTranslatedTriangles());
		}
		//CAMERA HARD CODE DISABLED!!!!! TODO
		if(false && this.camera != undefined){
			for(var i=0; i<triangles.length; i++){
				for(var j=0; j<3; j++){
					this.camera.project(triangles[i].vertices[j]);
				}
			}
		}
		return triangles;
	}

	draw(wireframe=false, aa=false, culling=false){
		this.bufferctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

		//get all triangles of scene
		var transtris;
		if(culling){
			transtris = this.getUnculledTranslatedTriangles();
		}else{
			transtris = this.getTranslatedTriangles();
		}
		if(wireframe){
			for(var i=0; i<transtris.length; i++){
				this.bufferctx.strokeStyle = "#" + transtris[i].color.toString(16);
				this.bufferctx.beginPath();
				this.bufferctx.moveTo(transtris[i].vertices[0].x, transtris[i].vertices[0].y);
				for(var j=1; j<4; j++){
					this.bufferctx.lineTo(transtris[i].vertices[j%3].x, transtris[i].vertices[j%3].y);
				}
				this.bufferctx.stroke();
			}
		}else{
			for(var i=0; i<this.zBuffers.length; i++){
				this.drawdata.set(this.zBuffers[i].populateFromGeometry(transtris, this.c.rays)[0]);
			}

			var imageData = this.bufferctx.getImageData(0, 0, this.bufferctx.canvas.width, this.bufferctx.canvas.height);
			imageData.data.set(this.drawdata);

			this.bufferctx.putImageData(imageData, 0, 0);
		}

		this.ctx.drawImage(this.bufferctx.canvas, 0, 0);
		//console.log("Total: ", performance.now() - p, " ms.");
	}
}

export {World};
