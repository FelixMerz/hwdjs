import {Vec3} from "./Vec3.js";
import {Quaternion} from "./Quaternion.js";
import {BoundingBox} from "./BoundingBox.js";
import {Tri} from "./Tri.js";
import {rgbToCol} from "./Color.js";

class Mesh{
	constructor(lightDirection){
		this.lightDirection = lightDirection;
		this.orientation = new Quaternion(1, 0, 0, 0);
		this.origin = new Vec3(0, 0, 0);
		this.triangles = [];
		this.translatedTriangles = [];
		this.needsTranslation = true;
		this.shadeVal = 1;
		this.vertexesChanged = true;
		this.vertexes = [];
	}

	draw(ctx){
		//var mat = new Quaternion(1, 0, 0, 0).multiply(this.orientation).toMatrix();
		var mat = this.orientation.toMatrix();
		ctx.fillStyle = "white";
		ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
		for(var i=0; i<this.triangles.length; i++){
			ctx.fillStyle = this.triangles[i].color;
			var rot = this.triangles[i].rotateByMatrix(mat);
			for(var j=0; j<3; j++){
				ctx.fillRect(this.origin.x + rot.vertices[j].x-1, this.origin.y + rot.vertices[j].y-1, 2, 2);
			}
		}
	}

	mergeVertices(){
		for(var i=0; i<this.triangles.length; i++){
			for(var j=0; j<this.triangles.length; j++){
				if(i!=j){
					for(var k=0; k<3; k++){
						var index = this.triangles[i].getVertexIndex(this.triangles[j].vertices[k]);
						if(index != -1){
							this.triangles[i].vertices[index] = this.triangles[j].vertices[k];
						}
					}
				}
			}
		}
        this.needsTranslation = true;
	}

	scale(n){
		var vertices = this.getVertices();
		for(var i=0; i<vertices.length; i++){
            var scaled = vertices[i].scale(n);
			vertices[i].x = scaled.x;
			vertices[i].y = scaled.y;
			vertices[i].z = scaled.z;
		}
        this.needsTranslation = true;
		return this;//temporary fix until decision is made about mutability
	}

	//moves the mesh by moving the origin
	move(v){
		this.origin = this.origin.add(v);
        this.needsTranslation = true;
		return this;//temporary fix until decision is made about mutability
	}

	//!!EXPENSIVE: moves the individual vertices WITHOUT moving the origin, keeps references to vertices globally intact
	shift(v){
		var vertices = this.getVertices();
		for(var i=0; i<vertices.length; i++){
			vertices[i].x += v.x;
			vertices[i].y += v.y;
			vertices[i].z += v.z;
		}
        this.needsTranslation = true;
		return this;
	}

	//optimization hack, TODO: create Vertices class instead of Vec3
	getVertices(){
		var r = [];
		var t = new Array(this.triangles.length * 3);
		var hash = Math.random();
		for(var i=0; i<this.triangles.length; i++){
			for(var j=0; j<3; j++){
				this.triangles[i].vertices[j].hash = 0;
				t[j*this.triangles.length + i] = this.triangles[i].vertices[j];
			}
		}
		for(var i=0; i<t.length; i++){
			if(t[i].hash != hash){
				t[i].hash = hash;
				r.push(t[i]);
			}
		}
		return r;
	}

	getTranslatedTriangles(){
        if(this.needsTranslation){
            this.needsTranslation = false;
    		var triangles = new Array(this.triangles.length);
    		//var mat = new Quaternion(1, 0, 0, 0).multiply(this.orientation).toMatrix();
			var mat = this.orientation.toMatrix();
			for(var i=0; i<this.triangles.length; i++){
				var t = this.triangles[i].rotateByMatrix(mat).move(this.origin);
    			t.color = this.triangles[i].color;
    			t.shade = this.triangles[i].shade;
    			triangles[i]=t;
    		}
            this.translatedTriangles = triangles;
        }
		return this.translatedTriangles;
	}

	getUnculledTranslatedTriangles(){
		var triangles = new Array();
		//var mat = new Quaternion(1, 0, 0, 0).multiply(this.orientation).toMatrix();
		var mat = this.orientation.toMatrix();
		for(var i=0; i<this.triangles.length; i++){
			var t = this.triangles[i].rotateByMatrix(mat).move(this.origin);
			//console.log(t.normal());
			if(Math.sign(t.normal().z) == -1){
				t.color = this.triangles[i].color;
				t.shade = this.triangles[i].shade;
				triangles.push(t);
			}
		}
        return triangles;
	}

	rotate(w, x, y, z){
		this.orientation = this.orientation.rotate(w, x, y, z);
        this.needsTranslation = true;
		this.shade();
		return this;//temporary yaddayadda
	}

	boundingBox(){
		var translatedMesh = new Mesh(null);
		translatedMesh.triangles = this.getTranslatedTriangles();
		var points = translatedMesh.getVertices();
		var xmin = points[0].x;
		var ymin = points[0].y;
		var zmin = points[0].z;
		var xmax = xmin;
		var ymax = ymin;
		var zmax = zmin;
		for(var i=0; i<points.length; i++){
			if(points[i].x < xmin) xmin = points[i].x;
			if(points[i].y < ymin) ymin = points[i].y;
			if(points[i].z < zmin) zmin = points[i].z;

			if(points[i].x > xmax) xmax = points[i].x;
			if(points[i].y > ymax) ymax = points[i].y;
			if(points[i].z > zmax) zmax = points[i].z;
		}
		return new BoundingBox(xmin, ymin, zmin, xmax, ymax, zmax);
	}

	shade(){
		var triangles = this.getTranslatedTriangles();
		for(var i=0; i<triangles.length; i++){
			var nshade = triangles[i].normal().normalize().z;
			this.triangles[i].shade = this.shadeVal*Math.abs(nshade)+(1-this.shadeVal);
		}
	}
	//TODO prevent creation of/remove inner faces
	extrude(tris, distance){
		var direction = new Vec3(0, 0, 0);
		var movedSurface = [];
		//calculate direction of extrusion
		for(var i=0; i<tris.length; i++){
			direction = direction.add(tris[i].normal());
		}
		direction = direction.normalize().scale(distance);
		for(var i=0; i<tris.length; i++){
			this.triangles = this.triangles.concat(tris[i].extrude(direction));
			movedSurface.push(this.triangles[this.triangles.length-1]);
		}
		console.log("final triangles", this.triangles);
		return movedSurface;
	}

	nExtrude(tris, distance){
		var direction = new Vec3(1, 0, 0);
		for(var i=0; i<tris.length; i++){
			direction = direction.add(tris[i].normal());
		}
		direction.x = direction.x / tris.length;
		direction.y = direction.y / tris.length;
		direction.z = direction.z / tris.length;
		direction = direction.normalize().scale(distance);
		var newSurface = [];
		for(var i=0; i<tris.length; i++){
			tris[i].color = rgbToCol(255, 0, 0);
			var t = new Tri(tris[i].vertices[0].add(direction), tris[i].vertices[1].add(direction), tris[i].vertices[2].add(direction));
			t.color = rgbToCol(255, 0, 0);
			newSurface.push(t);
		}
		console.log(newSurface);
		return tris.concat(newSurface);
	}
}

export {Mesh};
