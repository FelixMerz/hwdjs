import {Vec2} from "./Vec2.js";

class ZBuffer{
	constructor(w, h, aaLevel, yoff = 0){
		this.w = w;
		this.h = h;
		this.z = new Array(this.w*this.h);
		this.colors = new Uint32Array(this.w*this.h).fill(0x0a141eff);
		this.dim = this.w*this.h*4;
		this.yoff = yoff;
	}

	populateFromGeometry(tris){
		var actualIterations = 0;
		var buf = new ArrayBuffer(this.dim);
		this.colors = new Uint32Array(buf).fill(BACKGROUND_COLOR);//0a141eff)
		this.z.fill(MAX_DISTANCE);
		for(var t=0; t<tris.length; t++){
			var bb = tris[t].boundingBox();
			var xmin = bb.xmin-1;
			var xmax = bb.xmax+1;
			var ymin = bb.ymin-1;
			var ymax = bb.ymax+1;
			if(bb.xmax > 0 && bb.ymax > this.yoff && bb.xmin < this.w && bb.ymin < this.h+this.yoff){
				var closestZ = tris[t].closestZ();
				if(bb.xmin < 0)
					xmin = 0;
				if(bb.xmax > this.w)
					xmax = this.w;
				if(bb.ymin < this.yoff)
					ymin = this.yoff;
				if(bb.ymax > this.h+this.yoff)
					ymax = this.h+this.yoff;



				for(var j=ymin; j<ymax; j++){
					var partialZind = (j+this.yoff)*this.w;
					for(var i=xmin; i<xmax; i++){
						actualIterations++;
						//here
						var zind = partialZind + i;
						if(closestZ < this.z[zind]){
							if(tris[t].hasPoint(i, j)){
								var z = tris[t].getZ(i, j);
								if(z < this.z[zind]){
									this.z[zind] = z;
									var r = ((tris[t].color) & 0x000000ff) * tris[t].shade;
									var g = ((tris[t].color >> 8) & 0x000000ff) * tris[t].shade;
									var b = ((tris[t].color >> 16) & 0x000000ff) * tris[t].shade;
									this.colors[zind] = 0xff000000 | (b << 16) | (g << 8) | r;
								}
							}
						}
					}
				}
			}
			//combine colors
			if(this.aa && aa){
				for(var ind=0; ind < this.w*this.h; ind++){
					var r = this.colors[0][ind] & 0x000000ff;
					var g = (this.colors[0][ind] >> 8) & 0x000000ff;
					var b = (this.colors[0][ind] >> 16) & 0x000000ff;
					this.colors[0][ind] = 0xff000000 | ((b << 16) | g << 8) | r;
				}
			}
		}
		console.log("Actual Iterations:", actualIterations, "(" + actualIterations/1000000 + "M)");
		return [new Uint8ClampedArray(buf), this.yoff];
	}
}

export {ZBuffer};
