class BoundingBox{
	constructor(xmin, ymin, zmin, xmax, ymax, zmax){
		this.xmin = Math.floor(xmin)-1;
		this.ymin = Math.floor(ymin)-1;
		this.zmin = Math.floor(zmin)-1;
		this.xmax = Math.round(xmax)+1;
		this.ymax = Math.round(ymax)+1;
		this.zmax = Math.round(zmax)+1;
	}
}

export {BoundingBox};
