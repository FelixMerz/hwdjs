self.onmessage = function(message){
    var data = JSON.parse(message);
    var tris = new Array(data.tris.length);
    /* "constructor" */
    var z = [new Array(w*h)];
    var colors = [];
    var colors.push(new Uint32Array(data.w*data.h).fill(0x0a141eff));
    //this.colors = [];
    var dim = data.w*data.h*4;
    var aaOff = 0.4;
    if(data.aa){
        for(var i=0; i<this.aaLevel-1; i++){
            this.z.push(new Array(this.w*this.h));
            this.colors.push(new Uint32Array(this.w*this.h).fill(0x0a141eff));
        }
    }
    /**/

    var buf = [new ArrayBuffer(dim)];
    colors = [new Uint32Array(buf[0]).fill(BACKGROUND_COLOR)];//0a141eff)
    var z[0].fill(MAX_DISTANCE);
    if(data.aa){
        for(var i=0; i<data.aaLevel-1; i++){
            buf.push(new ArrayBuffer(dim));
            colors.push(new Uint32Array(buf[i+1]).fill(BACKGROUND_COLOR));
            z[i+1].fill(MAX_DISTANCE);
        }
    }
    var aait = 1;
    if(data.aa){
        aait = data.aaLevel;
    }
    var aaOffVec = new Vec2(0, 0);
    if(data.aa){
        aaOffVec = new Vec2(-0.707 * this.aaOff, -0.707 * this.aaOff);
    }
    for(var aac=0; aac < aait; aac++){
        aaOffVec = aaOffVec.rotate(360/data.aaLevel);
        for(var t=0; t<tris.length; t++){
                var closestZ = tris[t].closestZ();

                var bb = tris[t].boundingBox();
                var xmin = bb.xmin-1;
                var xmax = bb.xmax+1;
                var ymin = bb.ymin-1;
                var ymax = bb.ymax+1;


                if(bb.xmax > 0 && bb.ymax > this.yoff && bb.xmin < this.w && bb.ymin < this.h+this.yoff){
                    if(bb.xmin < 0)
                        xmin = 0;
                    if(bb.xmax > data.w)
                        xmax = data.w;
                    if(bb.ymin < data.yoffset)
                        ymin = data.yoffset;
                    if(bb.ymax > data.h+data.yoffset)
                        ymax = data.h+data.yoffset;



                    for(var j=ymin; j<ymax; j++){
                    var partialZind = (j+data.yoffset)*data.w;
                        for(var i=xmin; i<xmax; i++){
                            //here
                            var zind = partialZind + i;
                            if(closestZ < z[aac][zind]){
                                if(tris[t].hasPoint(i+aaOffVec.x, j+aaOffVec.y)){
                                    var z = tris[t].getZ(i+aaOffVec.x, j+aaOffVec.y);
                                    if(z < z[aac][zind]){
                                        z[aac][zind] = z;
                                        var r = ((tris[t].color) & 0x000000ff) * tris[t].shade;
                                        var g = ((tris[t].color >> 8) & 0x000000ff) * tris[t].shade;
                                        var b = ((tris[t].color >> 16) & 0x000000ff) * tris[t].shade;
                                        this.colors[aac][zind] = 0xff000000 | (b << 16) | (g << 8) | r;
                                    }
                                }
                            }
                        }
                    }
                }
        }
    }
    //combine colors
    if(this.aa && aa){
        for(var ind=0; ind < this.w*this.h; ind++){
            var r = this.colors[0][ind] & 0x000000ff;
            var g = (this.colors[0][ind] >> 8) & 0x000000ff;
            var b = (this.colors[0][ind] >> 16) & 0x000000ff;
            for(var i=1; i<this.aaLevel; i++){
                r += this.colors[i][ind] & 0x000000ff;
                g += (this.colors[i][ind] >> 8) & 0x000000ff;
                b += (this.colors[i][ind] >> 16) & 0x000000ff;
            }
            this.colors[0][ind] = 0xff000000 | (Math.floor(b/this.aaLevel) << 16) | (Math.floor(g/this.aaLevel) << 8) | Math.floor(r/this.aaLevel);
        }
    }

    return [new Uint8ClampedArray(buf[0]), data.yoff];
}


/*
var message = {tris: tris
              ,aa: this.aa
              ,aaLevel: this.aaLevel
              ,yoffset: yoffset
              ,w: this.w
              ,h: height
};
*/
