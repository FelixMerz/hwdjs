class Vec2{
	constructor(x, y){
		this.x = x;
		this.y = y;
	}

	rotate(d){
		var rad = d/180*Math.PI;
		return new Vec2(Math.cos(rad)*this.x-Math.sin(rad)*this.y, Math.sin(rad)*this.x + Math.cos(rad)*this.y);
	}

	magnitude(){
		return Math.sqrt(this.x*this.x + this.y*this.y);
	}

	normalize(){
		var l = this.magnitude();
		return new Vec2(this.x/l, this.y/l);
	}

	add(vec){
		return new Vec2(this.x+vec.x, this.y+vec.y);
	}

	sub(vec){
		return new Vec2(this.x-vec.x, this.y-vec.y);
	}

	multiply(n){
		return new Vec2(this.x*n, this.y*n);
	}

	angle(vec){
		return Math.acos(this.dot(vec)/(this.magnitude() * vec.magnitude()))*180/Math.PI;
	}

	dot(vec){
		return this.x*vec.x + this.y*vec.y;
	}

	perpDot(vec){
		return this.x * vec.y - this.y * vec.x;
	}

	distance(v){
		return Math.sqrt((this.x-v.x)*(this.x-v.x) + (this.y-v.y)*(this.y-v.y));
	}
}

export {Vec2};
