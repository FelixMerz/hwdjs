import {World} from "./classes/World.js";
import {Terrain} from "./classes/Terrain.js";
import {Noise} from "./classes/Noise.js";
import {Vec2} from "./classes/Vec2.js";
import {Vec3} from "./classes/Vec3.js";
import {Mesh} from "./classes/Mesh.js";
import {UnitCube} from "./classes/UnitCube.js";
import {RandomWalk} from "./classes/RandomWalk.js";
import {Camera} from "./classes/Camera.js";
import {Tri} from "./classes/Tri.js";
import {Quaternion} from "./classes/Quaternion.js";

window.MAX_DISTANCE = 10000;
window.BACKGROUND_COLOR = 0xff000000;//0xff777777;

window.world = new World(400, 200);
window.world.c.origin = new Vec3(0, -60, 0);
window.world.c.rays = window.world.c.generateRays();

//var cube = new RandomWalk(4, 3).scale(8);
var cube = new UnitCube().scale(10);
//cube.move(new Vec3(3, 30, 4));
//cube.rotate(40, 1, 0, 0);
//cube.rotate(40, 0, 1, 0);
//cube.rotate(40, 0, 0, 1);
//cube.move(new Vec3(10, 0, 0));



//scale, depth, seed, amplification, offset, repeatAfter
var noise = new Noise(0.01, 10, 123123, 50, new Vec2(3, 1), 512);
var terrain = new Terrain(20, 10, 10, noise);
terrain.move(new Vec3(0, 200, 30));
terrain.rotate(90, 1, 0, 0);
//terrain.extrude([terrain.triangles[10], terrain.triangles[20]], 25);

//window.world.meshes.push(cube);
window.world.meshes.push(terrain);
console.log("start");
window.setInterval(()=>{
//	console.log("frame");
	world.meshes[0].rotate(-15, 0, 1, 0);
	//world.meshes[0].rotate(3, 0, 1, 0);
	var p = performance.now();
	window.world.draw(!true, true);
	console.log("time:", performance.now()-p);
}, 20);

//basic "controls"
window.addEventListener("keydown", (e)=>{
	switch(e.keyCode){
		case 87:
			world.c.origin = world.c.origin.add(world.c.towards.normalize().scale(10));
			world.c.rays = world.c.generateRays();
			break;
		case 83:
			world.c.origin = world.c.origin.sub(world.c.towards.normalize().scale(10));
			world.c.rays = world.c.generateRays();
			break;
		case 65:
			world.c.origin = world.c.origin.add(new Vec3(10, 0, 0));
			world.c.rays = world.c.generateRays();
			break;
		case 68:
			world.c.origin = world.c.origin.sub(new Vec3(10, 0, 0));
			world.c.rays = world.c.generateRays();
			break;
		case 40:
			world.c.origin = world.c.origin.add(new Vec3(0, 0, 10));
			world.c.rays = world.c.generateRays();
			break;
		case 38:
			world.c.origin = world.c.origin.sub(new Vec3(0, 0, 10));
			world.c.rays = world.c.generateRays();
			break;
		default:
			console.log(e.keyCode);
	}
});

window.q = new Quaternion(0, 0, 1, 0);
window.v = new Vec3(0, 1, 0);
/*
var walk = new RandomWalk(20, 3, false, "uubbdlurrfddbbllddbb").scale(20);
walk.rotate(30, 1, 1, 1);
walk.move(new Vec3(2*c.width/4, c.height/2, 0));
window.world.meshes.push(walk);

//scale, depth, seed, amplification, offset, repeatAfter
var noise = new Noise(0.005, 10, 123123, 100, new Vec2(3, 1), 512);
var terrain = new Terrain(20, 10, 10, noise);
terrain.rotate(30, 1, 1, 1);
terrain.move(new Vec3(3*c.width/4, c.height/2, 0));
terrain.extrude([terrain.triangles[10], terrain.triangles[20]], 25);
window.world.meshes.push(terrain);
*/
//window.world.draw(!true, true);
